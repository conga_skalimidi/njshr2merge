trigger APTPS_AgreementLifecycleTrigger on Apttus__APTS_Agreement__c (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
    TriggerHandlerDispatcher.execute(Apttus__APTS_Agreement__c.getSObjectType());
}