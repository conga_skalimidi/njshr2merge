/************************************************************************************************************************
@Name: APTPS_Demo_Agreement_Automation
@Author: Conga PS Dev Team
@CreateDate: 20 May 2021
@Description: 
************************************************************************************************************************
@ModifiedBy:
@ModifiedDate:
@ChangeDescription:
************************************************************************************************************************/

public class APTPS_Demo_Agreement_Automation extends APTPS_CLM_Automation{
    public void activateAgreement(Id agreementId, Id docId){
        try{
            if(agreementId != null && docId != null) {
                String[] activateDocIds = new List<String>();
                activateDocIds.add(docId);
                String[] remDocIds = new String[]{};
                Boolean response = Apttus.AgreementWebService.activateAgreement(agreementId, activateDocIds, remDocIds);
                system.debug('Agreement activation response for agreement --> '+agreementId+' is '+response);
            }
        }catch(Exception e){
            system.debug('Error while activating the agreement '+agreementId+' Error details --> '+e.getMessage());
        }
    }
}