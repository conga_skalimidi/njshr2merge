public with sharing class APTPS_TailNumberController { 
    @AuraEnabled
    public static string getTails(Id agreementid){
        system.debug('agreementid in gettails '+agreementid);
        try {
            String tails = '';
            tails = getTailList(agreementid);
            return tails;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }
       
    public static String formatDate(Date d) {
    return d.year() + '-' + d.month() + '-' + d.day();
	}
    
    @AuraEnabled
    public static APTPS_Agreement_Extension__c populateTailInformation(String tailNumber,Id agreementid){
        try {
            //System.debug('tailNumberselected'+tailNumber);
            //System.debug('agreementid-->'+agreementid);
                if(agreementid != NULL){
                String aircraftType =''; 
                Id agreementExtId =null;
                Apttus__APTS_Agreement__c agreement = [SELECT id,Aircraft_Types__c,APTPS_Agreement_Extension__c,APTPS_Agreement_Extension__r.Name FROM Apttus__APTS_Agreement__c WHERE Id =:agreementid];
				aircraftType = 	agreement.Aircraft_Types__c;
                agreementExtId = agreement.APTPS_Agreement_Extension__c;  
                system.debug('aircraftType '+aircraftType);
                string aircraftTailjson = ACI_AircraftInformation.getAircraftTailInformation(tailNumber);
                system.debug('aircraftTailjson '+aircraftTailjson);  
                    if(String.isBlank(aircraftTailjson)){
                        throw new AuraHandledException('Error occured while receiving JSON');
                    }
                if(agreementExtId != null && !String.isBlank(aircraftTailjson)){
                    APTPS_Agreement_Extension__c extrec = [SELECT Id, APTPS_Actual_Delivery_Date__c,
                                                             APTPS_Serial_Number__c, APTPS_Tail_Number__c,                                                           
                                                            APTPS_Vintage__c, APTPS_Warranty_Date__c,APTPS_Legal_Name__c,
                          									APTPS_Manufacturer__c,APTPS_Number_of_Engines__c,APTPS_Cabin_Class_Size__c  
                                                           FROM APTPS_Agreement_Extension__c WHERE Id =:agreementExtId];
                    System.debug('extrec-->'+extrec);
                    Map<String, Object> airTail = (Map<String, Object>)JSON.deserializeUntyped(aircraftTailjson);
                    
                    Map<String, Object> airTail2 = (Map<String, Object>) airTail.get('TailInformation');
                    system.debug('airTail2 '+airTail2);
                    
                    Date actualDelDate=null;
                    Date warrantyDate = null; String serialNum = ''; String tailNum = ''; 
                    String legalName = ''; String manufacturer = ''; Integer noOfEngines =null; 
                    String cabinClassSize = '';
                  
                    System.debug('Tail number ID '+airTail2.get('ID'));
                    
                    //Access Aicraft Tail Data
                    if(!String.isBlank((String)airTail2.get('ActualDeliveryDate'))){
                    String strdate = (String)airTail2.get('ActualDeliveryDate');
					Date newDate = Date.parse(strdate);
					Datetime dt = Datetime.newInstanceGMT(newDate.year(),NewDate.month(),newDate.day());
                    Date myDate = date.newinstance(dt.year(), dt.month(), dt.day());
                    actualDelDate = myDate;
                    system.debug('actualDelDate '+actualDelDate);
                    }
                    if(!String.isBlank((String)airTail2.get('WarrantyDate'))){
                        String strdate = (String)airTail2.get('WarrantyDate');
						Date newDate = Date.parse(strdate);
						Datetime dt = Datetime.newInstanceGMT(newDate.year(),NewDate.month(),newDate.day());
                        Date myDate = date.newinstance(dt.year(), dt.month(), dt.day());
                        warrantyDate = myDate;
                    //warrantyDate = date.valueOf(formatDate((Date)airTail2.get('WarrantyDate')));
                        system.debug('warrantyDate '+warrantyDate); }
                    if(!String.isBlank((String)airTail2.get('SerialNumber'))){
                    serialNum = string.valueOf(airTail2.get('SerialNumber')); 
                        system.debug('serialNum '+serialNum); }
                    if(!String.isBlank((String)airTail2.get('TailNumber'))){
                    tailNum = string.valueOf(airTail2.get('TailNumber')); 
                        system.debug('tailNum '+tailNum); }
                    
                    	if(airTail2.get('AircraftTypeName')!=''){                        
                            legalName = string.valueOf(airTail2.get('AircraftTypeName'));
                    		system.debug('Legal_Name__c '+legalName); }
                        if(airTail2.get('Manufacturer')!=''){
                            manufacturer = string.valueOf(airTail2.get('Manufacturer'));
                            system.debug('Manufacturer '+manufacturer); 
                        }
                        if(airTail2.get('NoOfEngines')!=''){
                            noOfEngines = integer.valueOf(airTail2.get('NoOfEngines'));
                            system.debug('noOfEngines '+noOfEngines); 
                        }
                        if(airTail2.get('CabinClassSize')!=''){
                            cabinClassSize =  string.valueOf(airTail2.get('CabinClassSize'));
                            system.debug('cabinClassSize '+cabinClassSize); 
                        }
                //}
                    Aircraft_Tail__c aircraftTail = new Aircraft_Tail__c(Actual_Delivery_Date__c=actualDelDate,Warranty_Date__c=warrantyDate,Serial_Number__c=serialNum,Tail_Number__c=tailNum);
                    Aircraft_Type__c air_Type = new Aircraft_Type__c(Legal_Name__c=legalName,Manufacturer__c=manufacturer,Number_of_Engines__c=noOfEngines,Cabin_Class_Size__c=cabinClassSize);                                                  
                    populateAgreementExtension(aircraftTail,air_Type,extrec);                    
                }
                
                
            }
            return getAgreementExtension(agreementid);//JSON.serialize(tail)
        } catch (Exception e) {
            system.debug('error '+e.getMessage());
            throw new AuraHandledException(e.getMessage());
        }
    }
    public static String getTailList(Id agreementId){
        system.debug('agreementId in taillist '+agreementId);
        String returnTailList = '';
        String aircraftType ='',vintage=''; 
        if(agreementId!=null){        
        Apttus__APTS_Agreement__c agreement = [SELECT id,Aircraft_Types__c,APTPS_Agreement_Extension__c,APTPS_Agreement_Extension__r.Name,RecordType.Name FROM Apttus__APTS_Agreement__c WHERE Id =:agreementid];
		aircraftType = 	agreement.Aircraft_Types__c;
        system.debug('aircraftType in taillist '+aircraftType);
        Apttus__AgreementLineItem__c agrLI = [select id,Apttus_CMConfig__AttributeValueId__r.APTPS_Vintage__c from Apttus__AgreementLineItem__c where Apttus__AgreementId__c=:agreementId AND Apttus_CMConfig__OptionId__c!=null AND Apttus_CMConfig__IsPrimaryLine__c=true
                                              AND Apttus_CMConfig__OptionId__r.Family='Aircraft' limit 1];
            //Apttus_CMConfig__ChargeType__c='Purchase Price' OR Apttus_CMConfig__ChargeType__c='Lease Deposit'
        vintage = agrLI.Apttus_CMConfig__AttributeValueId__r.APTPS_Vintage__c;
        System.debug('vintage in tals '+vintage);
            if(String.isNotBlank(aircraftType)){
            if(agreement.RecordType.Name=='NJA Lease Purchase Agreement' || agreement.RecordType.Name=='NJE Lease Purchase Agreement'){
                returnTailList = ACI_AircraftInformation.getAircraftTailList(aircraftType,'');
            }else{
                returnTailList = ACI_AircraftInformation.getAircraftTailList(aircraftType,vintage);
            }
            }
       	
        System.debug('returnTailList '+returnTailList);
        }
                
        return returnTailList;
    }
    
     
    @AuraEnabled
    public static APTPS_Agreement_Extension__c getAgreementExtension(Id agreementid){
        APTPS_Agreement_Extension__c extrec = new APTPS_Agreement_Extension__c();
        if(agreementid != NULL){
            Id agreementExtId = [SELECT id,Name,APTPS_Agreement_Extension__c,APTPS_Agreement_Extension__r.Name FROM Apttus__APTS_Agreement__c WHERE Id =:agreementid].APTPS_Agreement_Extension__c;
            //System.debug('agreementExtId-->'+agreementExtId);
            if(agreementExtId != NULL){
                extrec = [SELECT Id, APTPS_Actual_Delivery_Date__c,                              
                          APTPS_Serial_Number__c, APTPS_Tail_Number__c, 
                          APTPS_Vintage__c, APTPS_Warranty_Date__c,APTPS_Legal_Name__c,
                          APTPS_Manufacturer__c,APTPS_Number_of_Engines__c,APTPS_Cabin_Class_Size__c
                          FROM APTPS_Agreement_Extension__c WHERE Id =:agreementExtId];
                System.debug('extrec-->'+extrec);
                return extrec;
            }
        }
        return extrec;
    }
    public static void populateAgreementExtension(Aircraft_Tail__c tail,Aircraft_Type__c airType,APTPS_Agreement_Extension__c agreementExt){
        if(tail != NULL && agreementExt != NULL){
            agreementExt.APTPS_Actual_Delivery_Date__c = tail.Actual_Delivery_Date__c;
            agreementExt.APTPS_Warranty_Date__c = tail.Warranty_Date__c;
            agreementExt.APTPS_Serial_Number__c = tail.Serial_Number__c;
            agreementExt.APTPS_Tail_Number__c = tail.Tail_Number__c;
            
            agreementExt.APTPS_Legal_Name__c = airType.Legal_Name__c;
            agreementExt.APTPS_Manufacturer__c = airType.Manufacturer__c;
            agreementExt.APTPS_Number_of_Engines__c = airType.Number_of_Engines__c;
            agreementExt.APTPS_Cabin_Class_Size__c = airType.Cabin_Class_Size__c;
            update agreementExt;
            system.debug('agreement extn updated '+agreementExt.id);
        }
    }
    
    
    @AuraEnabled
    public static Map<String,String> getData(Id agreementid)
    {
        try{
        Map<String,String> recordTypeMap = new Map<String,String>();
        Map<String,String> agrLeaseMap = new Map<String,String>();

        List<Apttus__APTS_Agreement__c> agrList = new List<Apttus__APTS_Agreement__c>();
        
   
        agrList = [select id,Apttus__Parent_Agreement__c,recordtype.name,Apttus_QPComply__RelatedProposalId__r.APTPS_Program_Type__c from Apttus__APTS_Agreement__c where Apttus__Parent_Agreement__c=:agreementid limit 1];
        if(agrList.size()>0){
            if(agrList[0].Apttus__Parent_Agreement__c!=null && (agrList[0].recordtype.name=='NJA Interim Lease Agreement' || 
              agrList[0].recordtype.name=='On Hold Interim Lease' || 
               agrList[0].recordtype.name=='NJE Interim Lease Agreement')){
                   agrLeaseMap.put(agrList[0].id,'childAgreement');
               }/*else if(agrList[0].Apttus_QPComply__RelatedProposalId__r.APTPS_Program_Type__c == 'Lease'){
                   agrLeaseMap.put('lease','lease');
               }*/
        }
        
        
        return agrLeaseMap;
        }
        catch (Exception e) {
            system.debug('error '+e.getMessage());
            throw new AuraHandledException(e.getMessage());
        }
    }   
                          
}