/************************************************************************************************************************
@Name: APTPS_CLMChildAgreementsInterface
@Author: Conga PS Dev Team
@CreateDate: 16 September 2021
@Description: 
************************************************************************************************************************
@ModifiedBy:
@ModifiedDate:
@ChangeDescription:
************************************************************************************************************************/

public interface APTPS_CLMChildAgreementsInterface {
    void updateChildAgreement(Set<String> args);
}