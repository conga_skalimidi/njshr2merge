/************************************************************************************************************************
@Name: APTPS_AgreementExtFieldsTest
@Author: Conga PS Dev Team
@CreateDate: 20 July 2021
@Description: 
************************************************************************************************************************
@ModifiedBy:
@ModifiedDate:
@ChangeDescription:
************************************************************************************************************************/

@isTest
public class APTPS_AgreementExtFieldsTest {
    @testSetup
    public static void makeData() {
        Test.startTest();
        //Create SNL Custom settings
        APTPS_SNL_Products__c snlProducts = new APTPS_SNL_Products__c();
        snlProducts.Products__c = 'Demo,Corporate Trial';
        insert snlProducts;
        
        Account accToInsert = new Account();
        accToInsert.Name = 'APTS Test Account';
        insert accToInsert;

        Service_Account__c serviceAcc = new Service_Account__c();
        serviceAcc.Account__c = accToInsert.Id;
        serviceAcc.NetJets_Company__c = 'NJA';
        serviceAcc.Status__c = 'Active';
        insert serviceAcc;

        Apttus_Config2__AccountLocation__c testAccLocation = APTS_CPQTestUtility.createAccountLocation('Loc1', accToInsert.Id);
        insert testAccLocation;
        
        Opportunity testOpportunity = APTS_CPQTestUtility.createOpportunity('test Apttus Opportunity', 'New Customer', accToInsert.Id, 'Propose');
        insert testOpportunity;
        
        Apttus_Config2__PriceList__c testPriceList = APTS_CPQTestUtility.createPriceList('test Apttus Price List', true);
        insert testPriceList;
        
        //Create Products
       
        List<Product2> prodList = new List<Product2>();
        //START: NJA Share product test setup
        Product2 NJA_SHARE = APTS_CPQTestUtility.createProduct('Share', 'NJUS_SHARE', 'Apttus', 'Bundle', true, true, true, true);
        prodList.add(NJA_SHARE);

        Product2 NJA_INCENTIVE = APTS_CPQTestUtility.createProduct('Purchase Price Incentives', APTS_ConstantUtil.PRICE_INCENTIVE_CODE, 'Apttus', 'Option', true, true, true, true);
        prodList.add(NJA_INCENTIVE);

        

        Product2 PHENOM_NJUS = APTS_CPQTestUtility.createProduct('Phenom 300', 'PHENOM_300_NJUS', 'Apttus', 'Option', true, true, true, true);
        PHENOM_NJUS.Family = 'Aircraft';
        prodList.add(PHENOM_NJUS);
        //END: NJA Share product test setup
        insert prodList;
        
        //Create Quote/Proposals
        List<Apttus_Proposal__Proposal__c> proposalList = new List<Apttus_Proposal__Proposal__c>();
        

        Apttus_Proposal__Proposal__c shareNJUSQuote = APTS_CPQTestUtility.createProposal('Share NJUS Quote', accToInsert.Id, testOpportunity.Id, 'Proposal', testPriceList.Id);
        shareNJUSQuote.APTS_New_Sale_or_Modification__c = APTS_Constants.NEW_SALE;
        shareNJUSQuote.APTPS_Program_Type__c = 'Share';
        shareNJUSQuote.CurrencyIsoCode = 'USD';
        proposalList.add(shareNJUSQuote);
        
        Apttus_Proposal__Proposal__c shareNJEQuote = APTS_CPQTestUtility.createProposal('Share NJE Quote', accToInsert.Id, testOpportunity.Id, 'Proposal', testPriceList.Id);
        shareNJEQuote.APTS_New_Sale_or_Modification__c = APTS_Constants.NEW_SALE;
        shareNJEQuote.APTPS_Program_Type__c = 'Share';
        shareNJEQuote.CurrencyIsoCode = 'EUR';
        proposalList.add(shareNJEQuote);
        insert proposalList;
        
        //Create Quote/Proposal Line Items
       
        //START: Share PLI test setup
        List<Apttus_Proposal__Proposal_Line_Item__c> pliList = new List<Apttus_Proposal__Proposal_Line_Item__c>();
        Apttus_Proposal__Proposal_Line_Item__c shareNJUSFET = APTS_CPQTestUtility.getPropLI(NJA_SHARE.Id, 'New', shareNJUSQuote.Id, '', null);
        shareNJUSFET.Apttus_QPConfig__LineType__c = APTS_ConstantUtil.LINE_TYPE;
        shareNJUSFET.Apttus_QPConfig__ChargeType__c = APTS_ConstantUtil.FEDERAL_EXCISE_TAX;
        shareNJUSFET.Apttus_QPConfig__IsPrimaryLine__c = TRUE;
        shareNJUSFET.Apttus_QPConfig__NetPrice__c = 100;
        pliList.add(shareNJUSFET);

        Apttus_Proposal__Proposal_Line_Item__c shareNJUSIncentive = APTS_CPQTestUtility.getPropLI(NJA_SHARE.Id, 'New', shareNJUSQuote.Id, '', null);
        shareNJUSIncentive.Apttus_QPConfig__LineType__c = APTS_ConstantUtil.OPTION;
        shareNJUSIncentive.Apttus_QPConfig__OptionId__c = NJA_INCENTIVE.Id;
        shareNJUSIncentive.Apttus_QPConfig__IsPrimaryLine__c = TRUE;
        pliList.add(shareNJUSIncentive);

        
        

        Apttus_Proposal__Proposal_Line_Item__c shareNJUSAc = APTS_CPQTestUtility.getPropLI(NJA_SHARE.Id, 'New', shareNJUSQuote.Id, '', null);
        shareNJUSAc.Apttus_QPConfig__ChargeType__c = APTS_ConstantUtil.PURCHASE_PRICE;
        shareNJUSAc.Apttus_QPConfig__IsPrimaryLine__c = TRUE;
        shareNJUSAc.Apttus_QPConfig__OptionId__c = PHENOM_NJUS.Id;
        shareNJUSAc.Apttus_QPConfig__NetPrice__c = 1000;
        pliList.add(shareNJUSAc);
        //END: Share PLI test setup
        insert pliList;
        
        //Create Proposal Attribute Value
       
        //START: Share PAV test setup
        list<Apttus_QPConfig__ProposalProductAttributeValue__c> pavList = new list<Apttus_QPConfig__ProposalProductAttributeValue__c>();
        Apttus_QPConfig__ProposalProductAttributeValue__c shareNJUSIncentivePAV = new Apttus_QPConfig__ProposalProductAttributeValue__c();
        shareNJUSIncentivePAV.Apttus_QPConfig__LineItemId__c = shareNJUSIncentive.Id;
        shareNJUSIncentivePAV.APTPS_Incentive_Amount__c = 1000;
        pavList.add(shareNJUSIncentivePAV);

       
        Apttus_QPConfig__ProposalProductAttributeValue__c shareNJUSAcPAV = new Apttus_QPConfig__ProposalProductAttributeValue__c();
        shareNJUSAcPAV.Apttus_QPConfig__LineItemId__c = shareNJUSAc.Id;
        shareNJUSAcPAV.APTPS_Vintage__c = '2015';
        shareNJUSAcPAV.APTPS_Hours__c = '50';
        shareNJUSAcPAV.APTPS_Percentage__c = 50;
        shareNJUSAcPAV.APTPS_Contract_Length__c = 60.0;
        shareNJUSAcPAV.APTPS_Delayed_Start_Date__c = Date.today();
        shareNJUSAcPAV.APTPS_Minimum_Commitment_Period__c = '36';
        shareNJUSAcPAV.APTPS_End_Date__c = Date.today();
        shareNJUSAcPAV.APTPS_Delayed_Start_Months__c = '2';
        pavList.add(shareNJUSAcPAV);
        //END: Share PAV test setup
        insert pavList;
        
        Apttus__APTS_Agreement__c shareCLMLifeCycleAg = new Apttus__APTS_Agreement__c();
        shareCLMLifeCycleAg.Name = 'CLM LifeCycle Share Agreement';
        shareCLMLifeCycleAg.Agreement_Status__c = 'Draft';
        shareCLMLifeCycleAg.Apttus__Account__c = accToInsert.Id;
        shareCLMLifeCycleAg.Apttus_QPComply__RelatedProposalId__c = shareNJUSQuote.Id;
        shareCLMLifeCycleAg.APTPS_Program_Type__c = APTS_ConstantUtil.SHARE;
        shareCLMLifeCycleAg.Apttus__Status__c = 'Request';
        shareCLMLifeCycleAg.Apttus__Status_Category__c = 'Request';
        shareCLMLifeCycleAg.CurrencyIsoCode = 'USD';
        
        
        insert shareCLMLifeCycleAg;

        Test.stopTest();
    }
    
    @isTest
    static void testShareCLM() {
        try{
            Apttus__APTS_Agreement__c ag = [SELECT Id, Agreement_Status__c, Chevron_Status__c, APTS_Path_Chevron_Status__c, Funding_Status__c, 
                                            Document_Status__c, Apttus_Approval__Approval_Status__c, APTPS_Owner_Signed__c, APTPS_Request_Aircraft_Positioning__c, 
                                            Accrual_Date__c, APTPS_Aircraft_Positioned_State__c, APTPS_Aircraft_Positioned_Date__c, Apttus__Status__c, Apttus__Status_Category__c 
                                            FROM Apttus__APTS_Agreement__c 
                                            WHERE CurrencyIsoCode = 'USD' AND APTPS_Program_Type__c =: APTS_ConstantUtil.SHARE LIMIT 1];
            
            if(ag != null) {
                //Document generated
                ag.Apttus__Status__c = APTS_ConstantUtil.READY_FOR_SIGN;
                ag.Apttus__Status_Category__c = APTS_ConstantUtil.IN_SIGN;
                update ag;
                
               
            }
        } catch(Exception e) {
            System.debug('APTPS_AgreementExtFieldsTest.testShareCLM --> Error while executing test class. Error details --> '+e);
        }
    }
    
    
}