/************************************************************************************************************************
@Name: APTPS_CPQFields_Lease
@Author: Conga PS Dev Team
@CreateDate: 3 Nov 2021
@Description: 
************************************************************************************************************************
@ModifiedBy:
@ModifiedDate:
@ChangeDescription:
************************************************************************************************************************/

public class APTPS_CPQFields_Lease {
public void populateShareFields(Apttus_Proposal__Proposal__c oProposal,List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines){
        Double leaseDeposit, MMF, OHF, SupplementalRate1,SupplementalRate2,incentiveFee,contractDeposit,percentage;
        String acName = null;
        boolean hasInterimLeaseOption = false;
        incentiveFee=contractDeposit=percentage=0;
        for (Apttus_Proposal__Proposal_Line_Item__c pli : quoteLines) {   
            if(APTS_ConstantUtil.AIRCRAFT.equalsIgnoreCase(pli.Product_Family__c) 
               && APTS_ConstantUtil.LEASE_DEPOSIT.equalsIgnoreCase(pli.Apttus_QPConfig__ChargeType__c)) {
                    acName = pli.Apttus_QPConfig__OptionId__r.Name;
                    percentage = pli.Apttus_QPConfig__AttributeValueId__r.APTPS_Percentage__c;
                    leaseDeposit = pli.Apttus_QPConfig__NetPrice__c;
            }
            if(APTS_ConstantUtil.OPTION.equalsIgnoreCase(pli.Apttus_QPConfig__LineType__c) 
               && APTS_ConstantUtil.MMF_CHARGE_TYPE.equalsIgnoreCase(pli.Apttus_QPConfig__ChargeType__c)) {
                   MMF = pli.Apttus_QPConfig__NetPrice__c;
            }
            if(APTS_ConstantUtil.OPTION.equalsIgnoreCase(pli.Apttus_QPConfig__LineType__c) 
               && APTS_ConstantUtil.OHF_CHARGE_TYPE.equalsIgnoreCase(pli.Apttus_QPConfig__ChargeType__c)) {
                   OHF = pli.Apttus_QPConfig__NetPrice__c;
            }
            if(APTS_ConstantUtil.OPTION.equalsIgnoreCase(pli.Apttus_QPConfig__LineType__c) 
               && APTS_ConstantUtil.SR1_CHARGE_TYPE.equalsIgnoreCase(pli.Apttus_QPConfig__ChargeType__c)) {
                   SupplementalRate1 = pli.Apttus_QPConfig__NetPrice__c;
            }
            if(APTS_ConstantUtil.OPTION.equalsIgnoreCase(pli.Apttus_QPConfig__LineType__c) 
               && APTS_ConstantUtil.SR2_CHARGE_TYPE.equalsIgnoreCase(pli.Apttus_QPConfig__ChargeType__c)) {
                   SupplementalRate2 = pli.Apttus_QPConfig__NetPrice__c;
            }
            
            if(pli.Apttus_QPConfig__IsPrimaryLine__c && APTS_ConstantUtil.OPTION.equalsIgnoreCase(pli.Apttus_QPConfig__LineType__c) 
                   && (APTS_ConstantUtil.PRICE_INCENTIVE_CODE.equalsIgnoreCase(pli.Apttus_QPConfig__OptionId__r.ProductCode) || 
                       APTS_ConstantUtil.PRICE_INCENTIVE_CODE_NJE.equalsIgnoreCase(pli.Apttus_QPConfig__OptionId__r.ProductCode))) {
                           incentiveFee = pli.Apttus_QPConfig__AttributeValueId__r.APTPS_Incentive_Amount__c;
            }
            if(pli.Apttus_QPConfig__IsPrimaryLine__c && APTS_ConstantUtil.OPTION.equalsIgnoreCase(pli.Apttus_QPConfig__LineType__c) 
                   && (APTS_ConstantUtil.INTERIM_LEASE_CODE.equalsIgnoreCase(pli.Apttus_QPConfig__OptionId__r.ProductCode) || 
                       APTS_ConstantUtil.INTERIM_LEASE_CODE_NJE.equalsIgnoreCase(pli.Apttus_QPConfig__OptionId__r.ProductCode))) {
                           contractDeposit = pli.Apttus_QPConfig__NetPrice__c;
                           hasInterimLeaseOption = true;
            }
        }      
        oProposal.Aircraft_Types__c = acName;
        oProposal.Product_Line__c = APTS_ConstantUtil.LEASE;
        oProposal.APTPS_MMF__c = MMF;
        oProposal.APTPS_OHF__c = OHF;
        oProposal.APTPS_Supplemental_Rate1__c = SupplementalRate1;
        oProposal.APTPS_Supplemental_Rate2__c = SupplementalRate2;
        oProposal.APTPS_Purchase_Price_Incentive__c = incentiveFee;    
        oProposal.APTPS_Contract_Purchase_Deposit__c = contractDeposit;
        oProposal.APTPS_Has_Interim_Lease_Option__c = hasInterimLeaseOption;
        oProposal.APTPS_Share_Percentage__c = percentage;
        oProposal.APTPS_Contract_Lease_Deposit__c = leaseDeposit;
        
        
         system.debug('oProposal-->'+oProposal);
    }   
   
    /** 
    @description: Demo NJE implementation
    @param:
    @return: 
    */
    public class NJE_Fields implements APTPS_CPQFieldsInterface {
        public void populateCPQFields(Map<String, Object> args, List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines) {
            system.debug('Updating Lease NJE CPQFields');
            Apttus_Proposal__Proposal__c proposalObj  = (Apttus_Proposal__Proposal__c)args.get('qId');
            APTPS_CPQFields_Lease obj = new APTPS_CPQFields_Lease();
            obj.populateShareFields(proposalObj,quoteLines);            
            
        }
    }
    
     /** 
    @description: Demo NJA implementation
    @param:
    @return: 
    */
    public class NJA_Fields implements APTPS_CPQFieldsInterface {
        public void populateCPQFields(Map<String, Object> args, List<Apttus_Proposal__Proposal_Line_Item__c> quoteLines) {
            system.debug('Updating Lease NJA CPQFields');
            Apttus_Proposal__Proposal__c proposalObj  = (Apttus_Proposal__Proposal__c)args.get('qId');
            APTPS_CPQFields_Lease obj = new APTPS_CPQFields_Lease();
            obj.populateShareFields(proposalObj,quoteLines);            
            
        }
    }
}