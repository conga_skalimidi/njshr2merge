/************************************************************************************************************************
@Name: APTPS_DealSummaryCallable
@Author: Conga PS Dev Team
@CreateDate: 17 May 2021
@Description: 
************************************************************************************************************************
@ModifiedBy:
@ModifiedDate:
@ChangeDescription:
************************************************************************************************************************/

public interface APTPS_DealSummaryCallable {
    Object call(Map<Id, String> quoteProductMap, Map<Id, Map<String, Object>> quoteArgMap);
}