/************************************************************************************************************************
@Name: APTPS_EmailNotification
@Author: Conga PS Dev Team
@CreateDate: 24 May 2021
@Description: 
************************************************************************************************************************
@ModifiedBy:
@ModifiedDate:
@ChangeDescription:
************************************************************************************************************************/

public class APTPS_EmailNotification implements APTPS_EmailNotificationCallable {
    private static String enMetadataName = 'APTPS_EN_Registry__mdt';
    public static Map<String, String> prodCodeClassNameMap = new Map<String, String>();
    
    /** 
    @description: Find out the APEX class implementation for the given productCode
    @param: Product Code
    @return: APEX Class name
    */
    private Map<String, Object> findEN_Implementation(Map<String, Object> args) {
        String Business_Object = (String)args.get('Business_Object');
        String Product_Code = (String)args.get('Product_Code');
        String Status = (String)args.get('Status');
        system.debug('Business_Object-->'+Business_Object+' Product_Code-->'+Product_Code+' Status-->'+Status);
        List<APTPS_EN_Registry__mdt> enRegistryList = [SELECT APTPS_Email_Template__c,APTPS_Business_Object__c, APTPS_Recipients__c,APTPS_Class_Name__c,APTPS_Additional_Emails__c, APTPS_From_Email_Address__c FROM APTPS_EN_Registry__mdt WHERE APTPS_Business_Object__c=:Business_Object AND APTPS_Status__c =:Status AND APTPS_Product_Code__c =:Product_Code];
        Map<String, Object> enArgs = new Map<String,Object>();
        if(!enRegistryList.isEmpty()) {
            enArgs.put('Id', args.get('Id'));
            enArgs.put('ContactId', args.get('ContactId'));
            enArgs.put('emailTemplate',enRegistryList[0].APTPS_Email_Template__c);
            enArgs.put('className',enRegistryList[0].APTPS_Class_Name__c);
            enArgs.put('recipients',enRegistryList[0].APTPS_Recipients__c);         
            enArgs.put('businessObject',enRegistryList[0].APTPS_Business_Object__c);            
            enArgs.put('additionalEmails',enRegistryList[0].APTPS_Additional_Emails__c);            
            enArgs.put('fromEmail',enRegistryList[0].APTPS_From_Email_Address__c); 
            enArgs.put('productCode',Product_Code);  
            enArgs.put('Status',Status);         
        }
        system.debug('enArgs-->'+enArgs);
        return enArgs;
    }
    
    /** 
    @description: Execute product specific Deal Summary logic.
    @param: Product code and arguments
    @return: 
    */
    public Object call(String productCode, Map<String, Object> args) {
        system.debug('Product Code to Send Email Notification --> '+productCode);
        system.debug('Arguments to call method --> '+args);
        String retMsg = null;
        Map<String, Object> enArgs = new Map<String,Object>();
        if(args == null || args.get('Id') == null) 
            throw new ExtensionMalformedCallException('Object Id is missing.');
       enArgs = findEN_Implementation(args);
        if(enArgs != null) {
            String className = (String) enArgs.get('className');
            APTPS_ENInterface enObj = (APTPS_ENInterface)Type.forName(className).newInstance();
            enObj.sendNotification(enArgs);
            retMsg = 'SUCCESS';
        }
        
        return retMsg;
    }
    
    /** 
    @description: Custom Exception implementation
    @param:
    @return: 
    */
    public class ExtensionMalformedCallException extends Exception {
        public String errMsg = '';
        public void ExtensionMalformedCallException(String errMsg) {
            this.errMsg = errMsg;
        }
    }
    
    /** 
    @description: Read Deal Summary Registry metadata and prepare the Map
    @param:
    @return: 
    */
   /* private void setProdCodeClassNameMap() {
        
        for(APTPS_EN_Registry__mdt handler : enHandlers) {
            if(handler.APTPS_Product_Code__c != null && handler.APTPS_Class_Name__c != null) 
                prodCodeClassNameMap.put(handler.APTPS_Product_Code__c, handler.APTPS_Class_Name__c);
        }
    }*/
    
}