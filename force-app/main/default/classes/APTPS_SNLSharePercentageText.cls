/************************************************************************************************************************
 @Author: Conga PS Dev Team
 @CreateDate:
 @Description: Used to get APTPS_Share_Percentage_Text values of APTPS_Share_Percentage from custom metadata 
 @Test Class: APTPS_SNLSharePercentageTextTest
************************************************************************************************************************/

public class APTPS_SNLSharePercentageText {
    public  static String populateAgreementExtFields(String PercentageValue){
        String spText = null;
        Map<String,APTPS_Share_Percentage_to_Text__mdt> sp = APTPS_Share_Percentage_to_Text__mdt.getAll();
        for(String spRec: sp.keySet()){
        if((sp.get(spRec).MasterLabel)==PercentageValue){
            spText = sp.get(spRec).Share_Percentage_Text__c;  
            return spText;
        }
        }  
        return spText;
    }
}