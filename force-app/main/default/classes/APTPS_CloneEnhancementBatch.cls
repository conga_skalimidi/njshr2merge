/************************************************************************************************************************
@Name: APTPS_CloneEnhancementBatch
@Author: Conga PS Dev Team
@CreateDate: 29 Oct 2021
@Description: Batch Class to Clone Selected SNL Enhancement
************************************************************************************************************************
@ModifiedBy:
@ModifiedDate:
@ChangeDescription:
************************************************************************************************************************/

public class APTPS_CloneEnhancementBatch implements Database.Batchable<sObject>{
    String configID;
    List<Apttus_Config2__LineItem__c> enhancementLines = null;
    public APTPS_CloneEnhancementBatch(String configID, List<Apttus_Config2__LineItem__c> lineItems) {
        this.configID = configID;
        this.enhancementLines = lineItems;
    }
    
    public List<Apttus_Config2__LineItem__c> start(Database.BatchableContext BC) {
        return enhancementLines;
    }
    
    public void execute(Database.BatchableContext BC, List<Apttus_Config2__LineItem__c> liList) {
        system.debug('Records to process --> '+liList.size());
        Map<Id, double> liPrimaryNumberMap = new Map<Id, double>();
        Map<double, Id> linePrimaryNumberLiMap = new Map<double, Id>();
        Map<Id, List<String>> liUniqueAgreementsMap = new Map<Id, List<String>>();
        Map<Id, List<String>> liUniqueQuotesMap = new Map<Id, List<String>>();
        Map<Id, Apttus_Config2__LineItem__c> liObjectMap = new Map<Id, Apttus_Config2__LineItem__c>();
        Map<Id, Id> liAttrMap = new Map<Id, Id>();
        List<String> globalQuoteList = new List<String>();
        List<String> globalAgList = new List<String>();
        for(Apttus_Config2__LineItem__c li : liList) {
            Boolean isProposalEntitlement = APTS_ConstantUtil.SNL_ENTITLEMENT_QUOTE.equalsIgnoreCase(li.Apttus_Config2__AttributeValueId__r.APTPS_Entitlement_Level__c) ? true : false;
            Boolean isAgreementEntitlement = APTS_ConstantUtil.SNL_ENTITLEMENT_AGR.equalsIgnoreCase(li.Apttus_Config2__AttributeValueId__r.APTPS_Entitlement_Level__c) ? true : false;

            //Proposal Entitlement
            if(isProposalEntitlement) {
                if(li.Apttus_Config2__AttributeValueId__c != null && 
                   li.Apttus_Config2__AttributeValueId__r.APTPS_Selected_Quote_Ids__c != null) {
                       List<String> uniqueList = new List<String>();
                       String selectedIds = li.Apttus_Config2__AttributeValueId__r.APTPS_Selected_Quote_Ids__c;
                       uniqueList = getUniqueList(selectedIds);
                       
                       if(uniqueList.size() > 1) {
                           liPrimaryNumberMap.put(li.Id, li.Apttus_Config2__PrimaryLineNumber__c);
                           liObjectMap.put(li.Id, li);
                           linePrimaryNumberLiMap.put(li.Apttus_Config2__PrimaryLineNumber__c, li.Id);
                           liUniqueQuotesMap.put(li.Id, uniqueList);
                           liAttrMap.put(li.Id, li.Apttus_Config2__AttributeValueId__c);
                           globalQuoteList.addAll(uniqueList);
                       } else 
                           system.debug('Duplicate Quote/Proposals are selected by user.');
                   }
            }
            
            //Agreement Entitlement
            if(isAgreementEntitlement) {
                if(li.Apttus_Config2__AttributeValueId__c != null && 
                   li.Apttus_Config2__AttributeValueId__r.APTPS_Selected_Agreement_Ids__c != null) {
                       List<String> uniqueList = new List<String>();
                       String selectedIds = li.Apttus_Config2__AttributeValueId__r.APTPS_Selected_Agreement_Ids__c;
                       uniqueList = getUniqueList(selectedIds);
                       
                       if(uniqueList.size() > 1) {
                           liPrimaryNumberMap.put(li.Id, li.Apttus_Config2__PrimaryLineNumber__c);
                           liObjectMap.put(li.Id, li);
                           linePrimaryNumberLiMap.put(li.Apttus_Config2__PrimaryLineNumber__c, li.Id);
                           liUniqueAgreementsMap.put(li.Id, uniqueList);
                           liAttrMap.put(li.Id, li.Apttus_Config2__AttributeValueId__c);
                           globalAgList.addAll(uniqueList);
                       } else 
                           system.debug('Duplicate Agreements are selected by user.');
                   }
            }
        } //end for loop
        system.debug('liPrimaryNumberMap --> '+liPrimaryNumberMap);
        system.debug('linePrimaryNumberLiMap --> '+linePrimaryNumberLiMap);
        system.debug('liUniqueAgreementsMap --> '+liUniqueAgreementsMap);
        system.debug('liUniqueQuotesMap --> '+liUniqueQuotesMap);
        system.debug('globalQuoteList --> '+globalQuoteList);
        system.debug('globalAgList --> '+globalAgList);
        
        //Get Agreement Details
        List<Apttus__APTS_Agreement__c> agList = [SELECT Id, Name, Apttus__FF_Agreement_Number__c 
                                                  FROM Apttus__APTS_Agreement__c 
                                                  WHERE Apttus__FF_Agreement_Number__c IN :globalAgList];
        system.debug('Agreement List --> '+agList);
        Map<String, Map<Id, String>> agDetails = new Map<String, Map<Id, String>>();
        Map<Id, String> agMap = new Map<Id, String>();
        for(Apttus__APTS_Agreement__c ag : agList) {
            agMap.put(ag.Id, ag.Name+' - '+ag.Apttus__FF_Agreement_Number__c);
            if(!agDetails.containsKey(ag.Apttus__FF_Agreement_Number__c))
                agDetails.put(ag.Apttus__FF_Agreement_Number__c, new Map<Id, String>{ag.Id => ag.Name+' - '+ag.Apttus__FF_Agreement_Number__c});
        }
        
        //Get Quote/Proposal Details
        List<Apttus_Proposal__Proposal__c> quoteList = [select Id, Name, Apttus_Proposal__Proposal_Name__c 
                                                        FROM Apttus_Proposal__Proposal__c 
                                                        WHERE Name IN :globalQuoteList];
        system.debug('Quote List --> '+quoteList);
        Map<String, Map<Id, String>> quoteDetails = new Map<String, Map<Id, String>>();
        Map<Id, String> quoteMap = new Map<Id, String>();
        for(Apttus_Proposal__Proposal__c q : quoteList) {
            quoteMap.put(q.Id, q.Apttus_Proposal__Proposal_Name__c+' - '+q.Name);
            if(!quoteDetails.containsKey(q.Name))
                quoteDetails.put(q.Name, new Map<Id, String>{q.Id => q.Apttus_Proposal__Proposal_Name__c+' - '+q.Name});
        }
        
        List<Apttus_Config2__ProductAttributeValue__c> liToUpdate = new List<Apttus_Config2__ProductAttributeValue__c>();
        //START: Clone Proposal Entitlement Line Items
        if(!liUniqueQuotesMap.isEmpty()) {
            liToUpdate = cloneLineItems(liAttrMap, liPrimaryNumberMap, liUniqueQuotesMap, this.configID, quoteDetails, liToUpdate, liObjectMap, APTS_ConstantUtil.SNL_ENTITLEMENT_QUOTE);
        }
        //END: Clone Proposal Entitlement Line Items
        
        //START: Clone Agreement Entitlement Line Items
        if(!liUniqueAgreementsMap.isEmpty()) {
            liToUpdate = cloneLineItems(liAttrMap, liPrimaryNumberMap, liUniqueAgreementsMap, this.configID, agDetails, liToUpdate, liObjectMap, APTS_ConstantUtil.SNL_ENTITLEMENT_AGR);
        }
        //END: Clone Agreement Entitlement Line Items
        
        if(!liToUpdate.isEmpty())
            update liToUpdate;
    }
    
    public void finish(Database.BatchableContext BC) {
        system.debug('Finish!');
        Apttus_Config2__ProductConfiguration__c configObj = new Apttus_Config2__ProductConfiguration__c(Id=configID);
        configObj.Apttus_Config2__Status__c = 'Saved';
        update configObj;
    }
    
    public List<String> getUniqueList(String selectedIds) {
        List<String> uniqueList = new List<String>();
        List<String> selectedAgList = selectedIds.split(',');
        if(selectedAgList.size() > 1) {
            for(String ag : selectedAgList) {
                if(uniqueList.isEmpty())
                    uniqueList.add(ag);
                else if(!uniqueList.contains(ag))
                    uniqueList.add(ag);
            }
        }
        return uniqueList;
    }
    
    public List<Apttus_Config2__ProductAttributeValue__c> cloneLineItems(Map<Id, Id> liAttrMap, Map<Id, double> liPrimaryNumberMap, 
                                                                         Map<Id, List<String>> liUniqueMap, Id configID, 
                                                                         Map<String, Map<Id, String>> lineDetails, 
                                                                         List<Apttus_Config2__ProductAttributeValue__c> liToUpdate, 
                                                                         Map<Id, Apttus_Config2__LineItem__c> liObjectMap, String entitlementType) {
        Map<double, String> lineItemMap = new Map<double, string>();
        if(!liAttrMap.isEmpty()) {
            List<Apttus_Config2__LineItem__c> liToDelete = new List<Apttus_Config2__LineItem__c>();
            if(!liPrimaryNumberMap.isEmpty()) {
                for(Id key : liPrimaryNumberMap.keySet()) {
                    Double lineNumber = liPrimaryNumberMap.get(key);
                    List<String> selectedRecords = liUniqueMap.get(key);
                    liToDelete.add(liObjectMap.get(key));
                    Integer listSize = selectedRecords.size();
                    system.debug('listSize, number of times to be cloned --> '+listSize);
                    for(integer i=0; i<listSize; i++) {
                        // Create the request object
                        List<Integer> primaryLineNumbers = new List<Integer>();
                        primaryLineNumbers.add(lineNumber.intValue());
                        Apttus_CPQApi.CPQ.CloneLineItemsRequestDO request = new Apttus_CPQApi.CPQ.CloneLineItemsRequestDO();
                        request.CartId = configID;
                        request.PrimaryLineNumbers = primaryLineNumbers;
                        
                        // Execute the cloneOptionLineItems routine
                        Apttus_CPQApi.CPQ.CloneLineItemsResponseDO response = Apttus_CPQApi.CPQWebService.cloneOptionLineItems(request);
                        for(Apttus_CPQApi.CPQ.IntegerMapDO intMapDO : response.OriginalToCloneMap)
                        {
                            system.debug('Responset Map --> '+intMapDO);
                            System.debug('Source Bundle Line Number = ' + intMapDO.Key + ', Cloned Bundle Line Number = ' + intMapDO.Value);
                            lineItemMap.put(intMapDO.Value, selectedRecords[0]);
                            selectedRecords.remove(0);
                        }
                    }
                }
            }
            
            if(!liToDelete.isEmpty())
                delete liToDelete;

            if(!lineItemMap.isEmpty()) {
                for(Apttus_Config2__LineItem__c li : [SELECT Id, Apttus_Config2__PrimaryLineNumber__c, Apttus_Config2__AttributeValueId__c, Apttus_Config2__AttributeValueId__r.APTPS_Selected_Agreement_Ids__c, 
                                                      Apttus_Config2__CopySourceNumber__c, 
                                                      Apttus_Config2__AttributeValueId__r.APTPS_Selected_Agreements__c, Apttus_Config2__AttributeValueId__r.APTPS_Agreement__c 
                                                      FROM Apttus_Config2__LineItem__c 
                                                      WHERE Apttus_Config2__ConfigurationId__c =: configID 
                                                      AND Apttus_Config2__PrimaryLineNumber__c IN : lineItemMap.keySet()]){
                                                          String uniqueId = lineItemMap.get(li.Apttus_Config2__PrimaryLineNumber__c);
                                                          Map<Id, String> liInfo = lineDetails.get(uniqueId);
                                                          Apttus_Config2__ProductAttributeValue__c attr = new Apttus_Config2__ProductAttributeValue__c(Id = li.Apttus_Config2__AttributeValueId__c);
                                                          Set<Id> keySetId = liInfo.keySet();
                                                          if(APTS_ConstantUtil.SNL_ENTITLEMENT_AGR.equalsIgnoreCase(entitlementType)) {
                                                              Id agId = (new list<Id>(keySetId))[0];
                                                              String agName = liInfo.get(agId);
                                                              attr.APTPS_Agreement__c = agId;
                                                              attr.APTPS_Selected_Agreement_Ids__c = uniqueId;
                                                              attr.APTPS_Selected_Agreements__c = agName;
                                                              attr.APTPS_Entitlement_Level__c = APTS_ConstantUtil.SNL_ENTITLEMENT_AGR;
                                                              attr.APTPS_Override_Agreement_selection__c = false;
                                                              attr.APTPS_Is_Generated_by_Clone_API__c = true;
                                                              liToUpdate.add(attr);
                                                          }
                                                          
                                                          if(APTS_ConstantUtil.SNL_ENTITLEMENT_QUOTE.equalsIgnoreCase(entitlementType)) {
                                                              Id qId = (new list<Id>(keySetId))[0];
                                                              String qName = liInfo.get(qId);
                                                              attr.APTPS_Quote_Proposal__c = qId;
                                                              attr.APTPS_Selected_Quote_Ids__c = uniqueId;
                                                              attr.APTPS_Selected_Quotes__c = qName;
                                                              attr.APTPS_Entitlement_Level__c = APTS_ConstantUtil.SNL_ENTITLEMENT_QUOTE;
                                                              attr.APTPS_Override_Quote_Proposal_Selection__c = false;
                                                              attr.APTPS_Is_Generated_by_Clone_API__c = true;
                                                              liToUpdate.add(attr);
                                                          }
                                                      }
            }
        }
        return liToUpdate;
    }
}