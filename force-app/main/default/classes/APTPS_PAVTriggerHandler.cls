/************************************************************************************************************************
@Name: APTPS_PAVTriggerHandler
@Author: Conga PS Dev Team
@CreateDate: 02 Nov 2021
@Description: Product Attribute Value Trigger Handler
************************************************************************************************************************
@ModifiedBy:
@ModifiedDate:
@ChangeDescription:
************************************************************************************************************************/

public class APTPS_PAVTriggerHandler extends TriggerHandler {
    public override void beforeInsert() {
        List<Apttus_Config2__ProductAttributeValue__c> newList = Trigger.new;
        List<Apttus_Config2__ProductAttributeValue__c> pavListToDefault = new List<Apttus_Config2__ProductAttributeValue__c>();
        //TODO: Add condition to identify eligible attribute records and introduce helper class.
        for(Apttus_Config2__ProductAttributeValue__c attr : newList) {
            //Reset Quote/Proposal Filter attributes
            attr.APTPS_Quote_Proposal__c = null;
            attr.APTPS_Selected_Quotes__c = '';
            attr.APTPS_Selected_Quote_Ids__c = '';
            attr.APTPS_Override_Quote_Proposal_Selection__c = true;
            
            //Reset Agreement Filter attributes
            attr.APTPS_Agreement__c = null;
            attr.APTPS_Selected_Agreements__c = '';
            attr.APTPS_Selected_Agreement_Ids__c = '';
            attr.APTPS_Override_Agreement_Selection__c = true;
            attr.APTPS_Is_Generated_By_Clone_API__c = false;
        }
    }
}