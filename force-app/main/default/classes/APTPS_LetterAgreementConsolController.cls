public with sharing class APTPS_LetterAgreementConsolController {
    public static Map<Integer, String> agreeCountMap = new Map<Integer, String> {
        1 => 'First', 2 => 'Second', 3 => 'Third', 4 => 'Fourth', 5 => 'Fifth', 6 => 'Sixth', 
        7 => 'Seventh', 8 => 'Eighth', 9 => 'Ninth', 10 => 'Tenth'
    };

    @AuraEnabled
    public static void createLAConsolidateRecords(String proposalId) {      
        system.debug('proposalId >> ' + proposalId);

        Apttus_Proposal__Proposal__c proposal = new Apttus_Proposal__Proposal__c(
            Id = proposalId,
            Proposal_Chevron_Status__c = 'LA Consolidation In Progress'
        );
        update proposal;

        processLACRecords(proposalId);
    }

    @future
    public static void processLACRecords(String proposalId) {   
        List<APTPS_Consolidate_Letter_Agreement__c> conLAList = 
            new List<APTPS_Consolidate_Letter_Agreement__c>();
        Set<String> validProductFamilies = new Set<String>();
        Map<String, Set<String>> validProdCodeAndChargeTypesMap = new Map<String, Set<String>>();
        List<Apttus_Proposal__Proposal_Line_Item__c> proposalLineItems = 
            new List<Apttus_Proposal__Proposal_Line_Item__c>();
        Set<Id> rejectedAssetId = new Set<Id>();
        Integer count = 0;
        Set<Id> processedIds = new Set<Id>();
        Id proposalAccountId;

        List<Apttus_Proposal__Proposal__c> proposals = [
            SELECT Id,
                Apttus_Proposal__Account__c
            FROM Apttus_Proposal__Proposal__c WHERE Id=:proposalId
        ];

        if(proposals.isEmpty()) 
            return ;

        DELETE [
            SELECT Id 
            FROM APTPS_Consolidate_Letter_Agreement__c 
            WHERE APTPS_Proposal__c =:proposalId
        ];

        List<APTPS_LA_Consolidation_Product_Mapping__mdt> laConsolProdMappingList = [
            SELECT Id, 
                APTPS_Charge_Type__c, 
                APTPS_Product_Code__c,
                APTPS_Product_Family__c
            FROM APTPS_LA_Consolidation_Product_Mapping__mdt
        ];

        if(!laConsolProdMappingList.isEmpty()) {
            for(APTPS_LA_Consolidation_Product_Mapping__mdt laCon : laConsolProdMappingList) {
                if(String.isNotBlank(laCon.APTPS_Product_Family__c)) {
                    validProductFamilies.add(laCon.APTPS_Product_Family__c.toLowerCase());
                }
                if(String.isNotBlank(laCon.APTPS_Product_Code__c) && laCon.APTPS_Charge_Type__c != null) {
                    Set<String> chargeTypeSet = new Set<String>();
                    for(String chargeType : laCon.APTPS_Charge_Type__c.split(',')) {
                        chargeTypeSet.add(chargeType.trim().toLowerCase());
                    }
                    validProdCodeAndChargeTypesMap.put(laCon.APTPS_Product_Code__c.toLowerCase(), chargeTypeSet);
                }
            }
        }

        for(Apttus_Proposal__Proposal_Line_Item__c proposalLI : [
            SELECT Id, Name, Apttus_QPConfig__OptionId__c, Apttus_Proposal__Product__c, 
                Apttus_Proposal__Proposal__c, Apttus_Proposal__Proposal__r.Apttus_Proposal__Account__c, 
                Apttus_QPConfig__ChargeType__c, Apttus_Proposal__Quantity__c,
                Apttus_Proposal__Product__r.ProductCode, Apttus_QPConfig__OptionId__r.ProductCode,
                Apttus_Proposal__Product__r.Family, Apttus_QPConfig__OptionId__r.Family,
                Apttus_QPConfig__AttributeValueId__r.APTPS_Hours__c,
                Apttus_Proposal__Proposal__r.APTPS_Share_Percentage__c,
                Apttus_Proposal__Proposal__r.Apttus_Proposal__Amount__c,                    
                Apttus_QPConfig__AttributeValueId__c, Apttus_QPConfig__ListPrice__c,
                Apttus_QPConfig__AssetLineItemId__c
            FROM Apttus_Proposal__Proposal_Line_Item__c
            WHERE Apttus_Proposal__Proposal__c =:proposalId
            AND Apttus_QPConfig__LineStatus__c != 'Cancelled'
        ]) {            

            proposalLineItems.add(proposalLI);
            if(proposalLI.Apttus_QPConfig__AssetLineItemId__c != null) {
                rejectedAssetId.add(proposalLI.Apttus_QPConfig__AssetLineItemId__c);
            }

        }
        system.debug('@21 proposalLineItems '+proposalLineItems);
        system.debug('@21 proposalLineItems '+proposalLineItems.size());

        for(Apttus_Config2__AssetLineItem__c assetLineItem : [
            SELECT Id, Name, Apttus_CMConfig__AgreementId__c, Apttus_Config2__ChargeType__c,
                Apttus_CMConfig__AgreementLineItemId__c, Asset_Agreement__c, 
                APTS_Original_Agreement__c, Apttus_Config2__BillToAccountId__c, 
                Apttus_Config2__AccountId__c, Apttus_Config2__ShipToAccountId__c,
                Apttus_Config2__ProductId__c, Apttus_Config2__OptionId__c,
                Apttus_CMConfig__AgreementId__r.Apttus__Contract_Start_Date__c,
                Apttus_CMConfig__AgreementId__r.APTPS_Share_Percentage__c,
                Apttus_CMConfig__AgreementId__r.Apttus__Total_Contract_Value__c,
                Apttus_CMConfig__AgreementId__r.Apttus__Contract_End_Date__c,
                Apttus_Config2__AttributeValueId__r.APTPS_Hours__c,
                Apttus_Config2__AttributeValueId__c, Apttus_Config2__ListPrice__c,
                Apttus_Config2__Quantity__c, Apttus_Config2__ProductId__r.ProductCode,
                Apttus_Config2__OptionId__r.ProductCode,
                Apttus_Config2__ProductId__r.Family, Apttus_Config2__OptionId__r.Family
            FROM Apttus_Config2__AssetLineItem__c 
            WHERE Apttus_CMConfig__AgreementId__c != null 
            AND Apttus_Config2__AccountId__c =:proposals[0].Apttus_Proposal__Account__c 
            AND Apttus_CMConfig__AgreementId__r.Apttus__Status_Category__c = 'In Effect'
            AND Apttus_CMConfig__AgreementId__r.Apttus__Status__c = 'Activated'
            AND Apttus_CMConfig__AgreementLineItemId__r.Apttus_CMConfig__LineStatus__c != 'Cancelled'
            ORDER BY Apttus_CMConfig__AgreementId__r.Apttus__Contract_Start_Date__c ASC
        ]) {
                                                                                                                        
            if(!rejectedAssetId.contains(assetLineItem.Id)) {

                if(
                    (
                        assetLineItem.Apttus_Config2__ProductId__c != null && 
                        String.isNotBlank(assetLineItem.Apttus_Config2__ProductId__r.Family) &&
                        validProductFamilies.contains(assetLineItem.Apttus_Config2__ProductId__r.Family.toLowerCase())
                    ) ||
                    (
                        assetLineItem.Apttus_Config2__OptionId__c != null && 
                        String.isNotBlank(assetLineItem.Apttus_Config2__OptionId__r.Family) &&
                        validProductFamilies.contains(assetLineItem.Apttus_Config2__OptionId__r.Family.toLowerCase())
                    ) ||
                    (
                        assetLineItem.Apttus_Config2__ProductId__c != null && 
                        String.isNotBlank(assetLineItem.Apttus_Config2__ProductId__r.ProductCode) &&
                        String.isNotBlank(assetLineItem.Apttus_Config2__ChargeType__c) && 
                        validProdCodeAndChargeTypesMap.containsKey(assetLineItem.Apttus_Config2__ProductId__r.ProductCode.toLowerCase()) &&
                        validProdCodeAndChargeTypesMap.get(
                            assetLineItem.Apttus_Config2__ProductId__r.ProductCode.toLowerCase()
                        ).contains(assetLineItem.Apttus_Config2__ChargeType__c.toLowerCase())                       
                    ) || 
                    (
                        assetLineItem.Apttus_Config2__OptionId__c != null && 
                        String.isNotBlank(assetLineItem.Apttus_Config2__OptionId__r.ProductCode) &&
                        String.isNotBlank(assetLineItem.Apttus_Config2__ChargeType__c) && 
                        validProdCodeAndChargeTypesMap.containsKey(assetLineItem.Apttus_Config2__OptionId__r.ProductCode.toLowerCase()) &&
                        validProdCodeAndChargeTypesMap.get(
                            assetLineItem.Apttus_Config2__OptionId__r.ProductCode.toLowerCase()
                        ).contains(assetLineItem.Apttus_Config2__ChargeType__c.toLowerCase())   
                    )
                ) {
                    
                    if(!processedIds.contains(assetLineItem.Apttus_CMConfig__AgreementId__c)){
                        count++;
                        processedIds.add(assetLineItem.Apttus_CMConfig__AgreementId__c);
                    }

                    APTPS_Consolidate_Letter_Agreement__c conLA = new APTPS_Consolidate_Letter_Agreement__c(
                        APTPS_Agreement_Number__c = assetLineItem.Apttus_CMConfig__AgreementId__c,
                        APTPS_Proposal__c = proposalId,
                        APTPS_Asset_Agreement__c = assetLineItem.Apttus_CMConfig__AgreementId__c,
                        APTPS_Is_Asset_Agreement__c = true
                    );
                    conLA.APTPS_Agreement__c = agreeCountMap.containsKey(count) ? agreeCountMap.get(count) : '';
                    conLA.APTPS_Agreement__c += ' Program Agreement';
                    if(assetLineItem.Apttus_CMConfig__AgreementId__r.Apttus__Contract_End_Date__c != null) {
                        conLA.APTPS_Agreement_End_Date__c = 
                            assetLineItem.Apttus_CMConfig__AgreementId__r.Apttus__Contract_End_Date__c;
                    }
                    if(assetLineItem.Apttus_CMConfig__AgreementId__r.Apttus__Contract_Start_Date__c != null) {
                        conLA.APTPS_Agreement_Start_Date__c = 
                            assetLineItem.Apttus_CMConfig__AgreementId__r.Apttus__Contract_Start_Date__c;
                    }
                    if(assetLineItem.Apttus_Config2__ChargeType__c != null) {
                        conLA.APTPS_Charge_Type__c = assetLineItem.Apttus_Config2__ChargeType__c;
                    }
                    if(assetLineItem.Apttus_Config2__AttributeValueId__c != null && 
                            assetLineItem.Apttus_Config2__AttributeValueId__r.APTPS_Hours__c != null) {
                        conLA.APTPS_Hours__c = assetLineItem.Apttus_Config2__AttributeValueId__r.APTPS_Hours__c;
                    }
                    if(assetLineItem.Apttus_Config2__ListPrice__c != null) {
                        conLA.APTPS_List_Price__c = assetLineItem.Apttus_Config2__ListPrice__c;
                    }
                    if(assetLineItem.Apttus_Config2__Quantity__c != null) {
                        conLA.APTPS_Quantity__c = assetLineItem.Apttus_Config2__Quantity__c;
                    }
                    if(assetLineItem.Apttus_Config2__ProductId__c != null) {
                        conLA.Product__c = assetLineItem.Apttus_Config2__ProductId__c;
                    }
                    if(assetLineItem.Apttus_Config2__OptionId__c != null) {
                        conLA.APTPS_Option__c = assetLineItem.Apttus_Config2__OptionId__c;
                    }
                    if(assetLineItem.Apttus_CMConfig__AgreementId__r.APTPS_Share_Percentage__c != null) {
                        conLA.APTPS_Share_Percentage__c = assetLineItem.Apttus_CMConfig__AgreementId__r.APTPS_Share_Percentage__c;
                    }if(assetLineItem.Apttus_CMConfig__AgreementId__r.Apttus__Total_Contract_Value__c != null) {
                        conLA.APTPS_Agreement_Total_Value__c = assetLineItem.Apttus_CMConfig__AgreementId__r.Apttus__Total_Contract_Value__c;
                    }
                    conLAList.add(conLA);
                }

            }
                
        }

        if(!proposalLineItems.isEmpty()) {
            for(Apttus_Proposal__Proposal_Line_Item__c proposalLI : proposalLineItems) {
                
                if(
                    (
                        proposalLI.Apttus_Proposal__Product__c != null && 
                        String.isNotBlank(proposalLI.Apttus_Proposal__Product__r.Family) &&
                        validProductFamilies.contains(proposalLI.Apttus_Proposal__Product__r.Family.toLowerCase())
                    ) ||
                    (
                        proposalLI.Apttus_QPConfig__OptionId__c != null && 
                        String.isNotBlank(proposalLI.Apttus_QPConfig__OptionId__r.Family) &&
                        validProductFamilies.contains(proposalLI.Apttus_QPConfig__OptionId__r.Family.toLowerCase())
                    ) ||
                    (
                        proposalLI.Apttus_Proposal__Product__c != null && 
                        String.isNotBlank(proposalLI.Apttus_Proposal__Product__r.ProductCode) &&
                        String.isNotBlank(proposalLI.Apttus_QPConfig__ChargeType__c) && 
                        validProdCodeAndChargeTypesMap.containsKey(proposalLI.Apttus_Proposal__Product__r.ProductCode.toLowerCase()) &&
                        validProdCodeAndChargeTypesMap.get(
                            proposalLI.Apttus_Proposal__Product__r.ProductCode.toLowerCase()
                        ).contains(proposalLI.Apttus_QPConfig__ChargeType__c.toLowerCase())                     
                    ) || 
                    (
                        proposalLI.Apttus_QPConfig__OptionId__c != null && 
                        String.isNotBlank(proposalLI.Apttus_QPConfig__OptionId__r.ProductCode) &&
                        String.isNotBlank(proposalLI.Apttus_QPConfig__ChargeType__c) && 
                        validProdCodeAndChargeTypesMap.containsKey(proposalLI.Apttus_QPConfig__OptionId__r.ProductCode.toLowerCase()) &&
                        validProdCodeAndChargeTypesMap.get(
                            proposalLI.Apttus_QPConfig__OptionId__r.ProductCode.toLowerCase()
                        ).contains(proposalLI.Apttus_QPConfig__ChargeType__c.toLowerCase()) 
                    )
                ) {
                    if(!processedIds.contains(proposalLI.Apttus_Proposal__Proposal__c)){
                        count++;
                        processedIds.add(proposalLI.Apttus_Proposal__Proposal__c);
                    }

                    APTPS_Consolidate_Letter_Agreement__c conLA = new APTPS_Consolidate_Letter_Agreement__c(
                        APTPS_Proposal__c = proposalLI.Apttus_Proposal__Proposal__c
                    );
                    conLA.APTPS_Agreement__c = agreeCountMap.containsKey(count) ? agreeCountMap.get(count) : '';
                    conLA.APTPS_Agreement__c += ' Program Agreement';
                    if(proposalLI.Apttus_QPConfig__ChargeType__c != null) {
                        conLA.APTPS_Charge_Type__c = proposalLI.Apttus_QPConfig__ChargeType__c;
                    }
                    if(proposalLI.Apttus_QPConfig__AttributeValueId__c != null && 
                        proposalLI.Apttus_QPConfig__AttributeValueId__r.APTPS_Hours__c != null) {
                        conLA.APTPS_Hours__c = proposalLI.Apttus_QPConfig__AttributeValueId__r.APTPS_Hours__c;
                    }
                    if(proposalLI.Apttus_QPConfig__ListPrice__c != null) {
                        conLA.APTPS_List_Price__c = proposalLI.Apttus_QPConfig__ListPrice__c;
                    }
                    if(proposalLI.Apttus_Proposal__Quantity__c != null) {
                        conLA.APTPS_Quantity__c = proposalLI.Apttus_Proposal__Quantity__c;
                    }
                    if(proposalLI.Apttus_Proposal__Product__c != null) {
                        conLA.Product__c = proposalLI.Apttus_Proposal__Product__c;
                    }
                    if(proposalLI.Apttus_QPConfig__OptionId__c != null) {
                        conLA.APTPS_Option__c = proposalLI.Apttus_QPConfig__OptionId__c;
                    }
                    if(proposalLI.Apttus_Proposal__Proposal__r.APTPS_Share_Percentage__c != null) {
                        conLA.APTPS_Share_Percentage__c = proposalLI.Apttus_Proposal__Proposal__r.APTPS_Share_Percentage__c;
                    }
                    if(proposalLI.Apttus_Proposal__Proposal__r.Apttus_Proposal__Amount__c != null) {
                        conLA.APTPS_Agreement_Total_Value__c = proposalLI.Apttus_Proposal__Proposal__r.Apttus_Proposal__Amount__c;
                    }
                    
                    conLAList.add(conLA);
                }
            }
        }
        
        if(!conLAList.isEmpty()) {
            INSERT conLAList;
            //Adding the records into a CSV and sending it via Email
            try{
            List<APTPS_Consolidate_Letter_Agreement__c> insertedconLAList = [SELECT Id,Product__r.Name,APTPS_Option__r.Name, Name, APTPS_Agreement_End_Date__c, APTPS_Agreement_Number__c, APTPS_Agreement_Start_Date__c, APTPS_Agreement__c,
                                                                             APTPS_Charge_Type__c, APTPS_Hours__c, APTPS_List_Price__c, APTPS_Option__c, APTPS_Proposal__c, APTPS_Quantity__c, Product__c, 
                                                                             APTPS_Related_Opportunity_Name__c,APTPS_Program__c, APTPS_Transaction_Type__c,APTPS_Share_Percentage__c,APTPS_Agreement_Total_Value__c  
            																FROM APTPS_Consolidate_Letter_Agreement__c 
            																WHERE APTPS_Proposal__c =:proposalId];
            String csvColumnHeader;
            List<String> csvRowValues = new List<String>();
            for(APTPS_Consolidate_Letter_Agreement__c objConLA : insertedconLAList){
                String Name = objConLA.Name != NULL ? objConLA.Name : '';
                String AgreementName = objConLA.APTPS_Agreement__c != NULL ? objConLA.APTPS_Agreement__c : '';
                Date StartDate = objConLA.APTPS_Agreement_Start_Date__c ;
                Date EndDate = objConLA.APTPS_Agreement_End_Date__c ;
                String ProductName = objConLA.APTPS_Option__c != NULL ? objConLA.APTPS_Option__r.Name : objConLA.Product__r.Name;
                String ChargeType = objConLA.APTPS_Charge_Type__c != NULL ? objConLA.APTPS_Charge_Type__c : '';
                String Hours = objConLA.APTPS_Hours__c != NULL ? objConLA.APTPS_Hours__c : '';
                String ListPrice = String.valueOf(objConLA.APTPS_List_Price__c) != NULL ? String.valueOf(objConLA.APTPS_List_Price__c) : '';
                String Quantity = String.valueOf(objConLA.APTPS_Quantity__c) != NULL ? String.valueOf(objConLA.APTPS_Quantity__c) : '';
                String sharePercent = String.valueOf(objConLA.APTPS_Share_Percentage__c) != NULL ? String.valueOf(objConLA.APTPS_Share_Percentage__c) : '';
                String totalValue = String.valueOf(objConLA.APTPS_Agreement_Total_Value__c) != NULL ? String.valueOf(objConLA.APTPS_Agreement_Total_Value__c) : '';
                String transactionType = String.valueOf(objConLA.APTPS_Transaction_Type__c) != NULL ? String.valueOf(objConLA.APTPS_Transaction_Type__c) : '';
                String program = String.valueOf(objConLA.APTPS_Program__c) != NULL ? String.valueOf(objConLA.APTPS_Program__c) : '';
                String optyname = String.valueOf(objConLA.APTPS_Related_Opportunity_Name__c) != NULL ? String.valueOf(objConLA.APTPS_Related_Opportunity_Name__c) : '';
                String rowStr = Name + ',' + AgreementName + ',' + StartDate + ',' + EndDate + ','+ ProductName + ','+ ChargeType + ','+ Hours + ','+ ListPrice + ','+ Quantity+ ','+ sharePercent+ ','+ totalValue+ ','+ transactionType+ ','+ program+ ','+ optyname;
                csvRowValues.add(rowStr);
            }
            csvColumnHeader = 'Name, Agreement Name, Start Date, End Date,Product Name,Charge Type,Hours,List Price, Quantity, Share Percentage, Agreement Total Value,Transaction Type, Program, opportunity Name\n';
            String csvFile = csvColumnHeader + String.join(csvRowValues,'\n');
            Apttus_Proposal__Proposal__c proposalrec = [SELECT Id,OwnerId FROM Apttus_Proposal__Proposal__c WHERE Id=:proposalId LIMIT 1];
            String sRecipantName = [Select id,Email from User where id =:proposalrec.OwnerId].Email ;//UserInfo.getUserEmail();
            String[] sendingTo = new String[]{sRecipantName};
            Messaging.EmailFileAttachment csvAttachment = new Messaging.EmailFileAttachment();
            Blob csvBlob = blob.valueOf(csvFile);
            String csvName = 'Consolidated Letter Agreement Records.csv';
                csvAttachment.setFileName(csvName);
            csvAttachment.setBody(csvBlob);
            csvAttachment.setContentType('text/csv');
            csvAttachment.contenttype ='application/vnd.ms-excel';
            Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
            String subject = 'Consolidated Letter Agreement Records';
            email.setSubject(subject);
            email.setSenderDisplayName('noreply@salesforce.com');
            email.setToAddresses(sendingTo);
            email.setPlainTextBody('Please find the Consolidated Letter Agreement Records attached');
                                   email.setFileAttachments(new Messaging.EmailFileAttachment[]{csvAttachment});
                                   Messaging.SendEmailResult[] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[]{email});
            }catch(Exception e){
            System.debug('Exception -->  message'+e.getMessage()+'Line number'+e.getLineNumber());
        }
        }

        Apttus_Proposal__Proposal__c proposal = new Apttus_Proposal__Proposal__c(
            Id = proposalId,
            Proposal_Chevron_Status__c = 'LA Consolidation Completed'
        );
        update proposal;
    }
}