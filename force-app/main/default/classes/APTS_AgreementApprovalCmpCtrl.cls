/***********************************************
 * Description : Class being used as controller for email template - Approval_Assignment_Template.
                 Called from Visualforce Component - APTS_AgreementApprovalNotificationCmp.
 *               Used to retrieve clauses related to the Approval request.
 * Author : Siva Kumar, 23-July-2021
 ***********************************************/
public class APTS_AgreementApprovalCmpCtrl {

    public Apttus_Approval__Approval_Request__c relatedTo{ get; set;}    
    public User recipient {get; set;}
    //public List<Apttus__Agreement_Clause__c> agreementClauses{get; set;}
    //public Map<String,String> approvalClauseMap{get; set;}
    public String baseurl = system.Url.getSalesforceBaseUrl().toExternalForm();
    public Apttus_Approval__Approval_Request__c apprlReq =  new Apttus_Approval__Approval_Request__c();
    
    public APTS_AgreementApprovalCmpCtrl(Apttus_Approval.AgreementApprovalEmailController agreementController) {  
                 
    }    
    
    public string getbaseURL(){
        return baseurl;
    }
    /*public List<Apttus__Agreement_Clause__c> getDetails()
    {
        
        if(relatedTo != null){
            agreementClauses = new List<Apttus__Agreement_Clause__c>();
            List<Apttus_Approval__Approval_Request__c> apprlReq = [SELECT Id,                        Apttus_Approval__Object_Id__c,                        Apttus_Approval__Actual_Approver__c ,Apttus_Approval__ChildObjectRefId__c,
            Apttus_Approval__Assigned_To_Id__c  
            FROM Apttus_Approval__Approval_Request__c 
            WHERE Id =: relatedTo.Id LIMIT 1];
            set<string> clauseIds = new set<string>();
            if(!apprlReq.isEmpty()) {
               approvalClauseMap = new Map<String,String>();
               system.debug('apprlReq[0].id-->'+apprlReq[0].id);
              for(Apttus_Approval__Approval_Request__c approvalReq: [SELECT Id,name,Apttus_Approval__ChildObjectRefId__c FROM Apttus_Approval__Approval_Request__c WHERE Apttus_Approval__ObjectRefId__c=:apprlReq[0].Apttus_Approval__Object_Id__c and Apttus_Approval__Approval_Status__c = 'Assigned' AND Apttus_Approval__Assigned_To_Id__c=:apprlReq[0].Apttus_Approval__Assigned_To_Id__c]) {
               // for(Apttus_Approval__Approval_Request__c approvalReq: [SELECT Id,name,Apttus_Approval__ChildObjectRefId__c FROM Apttus_Approval__Approval_Request__c WHERE id=:apprlReq[0].id ]) {
                    approvalClauseMap.put(approvalReq.Apttus_Approval__ChildObjectRefId__c,approvalReq.Id);
                    clauseIds.add(approvalReq.Apttus_Approval__ChildObjectRefId__c);
                }
               system.debug('clauseIds-->'+clauseIds);
               if(!clauseIds.isEmpty()) {
                    
                    agreementClauses = [SELECT  id,Apttus__Category__c,Apttus__Clause__c,Apttus__Subcategory__c,Apttus__Risk_Rating__c FROM Apttus__Agreement_Clause__c WHERE Id IN :clauseIds];
               }
                
               
            }
            
            
            
           
        }        

        return agreementClauses;
    }*/
    
    public PageReference getMyApprovalsPage()
    {
        String apttusPage = '/apex/Apttus_Approval__myapprovals?returnId=';
        Id returnId = apprlReq.Apttus_Approval__ObjectRefId__c;
        Pagereference pg = new Pagereference(baseurl + apttusPage + returnId + '&sObjectId=' + apprlReq.Apttus_Approval__ObjectRefId__c);
        return pg;
    }   
    
    public PageReference getAgreementPage()
    {
        String apttusPage = '/';
        Id agreementId = apprlReq.Apttus_Approval__ObjectRefId__c;
        Pagereference pg = new Pagereference(baseurl + apttusPage + agreementId );
        return pg;
    } 

    
}