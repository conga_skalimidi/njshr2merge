/************************************************************************************************************************
@Name: APTPS_SNLGenerateDocController
@Author: Conga PS Dev Team
@CreateDate: 11 Nov 2021
@Description: 
************************************************************************************************************************
@ModifiedBy:
@ModifiedDate:
@ChangeDescription:
************************************************************************************************************************/

public class APTPS_SNLGenerateDocController {
    public Id agId {get;set;}
    public String userAction {get;set;}
    public APTPS_SNLGenerateDocController() {
        this.agId = System.currentPageReference().getParameters().get('agId');
        this.userAction = System.currentPageReference().getParameters().get('action');
        system.debug('Agreement Id --> '+agId+' userAction --> '+userAction);
    }
    
    public PageReference init()
    {
        List<Apttus__AgreementLineItem__c> aliToUpdates = new List<Apttus__AgreementLineItem__c>();
        Map<String, Decimal> appliedOHFMap = new Map<String, Decimal>();
        Map<String, Decimal> allOHFLinesMap = new Map<String, Decimal>();
        Map<String, Id> ohfIdMap = new Map<String, Id>();
        Map<Integer, String> ohfPricingConfigMap = new Map<Integer, String>();
        List<String> applicableOHFList = new List<String>();
        Set<Id> ohfIdSet = new Set<Id>();
        Date warrantyDate = null;
        Integer numberOfMonths = 0;
        Decimal ohfAdjustmentAmount = 0;
        PageReference pg = null;
        if(agId != null){
            
            for(Apttus__AgreementLineItem__c ali : [SELECT Id, Apttus_CMConfig__ChargeType__c, Apttus__NetPrice__c, Apttus__AgreementId__r.APTPS_Agreement_Extension__r.APTPS_Warranty_Date__c, 
                                                    APTPS_Is_Included_In_OHF__c, APTPS_Is_OHF_Warranty_Date_Considered__c, Apttus_CMConfig__BasePriceOverride__c 
                                                    FROM Apttus__AgreementLineItem__c 
                                                    WHERE Apttus__AgreementId__c = :agId 
                                                    AND (Apttus_CMConfig__ChargeType__c = :APTS_ConstantUtil.OHF_CHARGE_TYPE 
                                                         OR Apttus_CMConfig__ChargeType__c = :APTS_ConstantUtil.OHF_CHARGE_TYPE_1 
                                                         OR Apttus_CMConfig__ChargeType__c = :APTS_ConstantUtil.OHF_CHARGE_TYPE_2 
                                                         OR Apttus_CMConfig__ChargeType__c = :APTS_ConstantUtil.OHF_CHARGE_TYPE_3 
                                                         OR Apttus_CMConfig__ChargeType__c = :APTS_ConstantUtil.OHF_CHARGE_TYPE_4)]) {
                                                             if(APTS_ConstantUtil.OHF_CHARGE_TYPE.equalsIgnoreCase(ali.Apttus_CMConfig__ChargeType__c)) 
                                                                 warrantyDate = ali.Apttus__AgreementId__r.APTPS_Agreement_Extension__c != null ? ali.Apttus__AgreementId__r.APTPS_Agreement_Extension__r.APTPS_Warranty_Date__c : null;
                                                             if(ali.APTPS_Is_Included_In_OHF__c)
                                                                 appliedOHFMap.put(ali.Apttus_CMConfig__ChargeType__c, ali.Apttus__NetPrice__c);
                                                             
                                                             allOHFLinesMap.put(ali.Apttus_CMConfig__ChargeType__c, ali.Apttus__NetPrice__c);
                                                             ohfIdMap.put(ali.Apttus_CMConfig__ChargeType__c, ali.Id);
                                                         }
            system.debug('warrantyDate --> '+warrantyDate+' appliedOHFMap --> '+appliedOHFMap);
            if(warrantyDate != null) {
                ohfPricingConfigMap = APTPS_SNL_Util.getOHFPricingConfig();
                numberOfMonths = APTPS_SNL_Util.calculateMonths(warrantyDate, Date.today());
                system.debug('Difference in terms of months --> '+numberOfMonths);
                //Iterate OHF Pricing Config
                for(Integer key : ohfPricingConfigMap.keySet()) {
                    if(key <= numberOfMonths && !applicableOHFList.contains(ohfPricingConfigMap.get(key))) {
                        applicableOHFList.add(ohfPricingConfigMap.get(key));
                    }
                }
                system.debug('applicableOHFList --> '+applicableOHFList);
                
                //Iterate applied OHF charge types/ALIs
                Integer i=0;
                if(!appliedOHFMap.isEmpty()) {
                    for(String pli : appliedOHFMap.keySet()) {
                        system.debug('Checking for --> '+pli);
                        if(!applicableOHFList.isEmpty() && applicableOHFList.contains(pli))
                            applicableOHFList.remove(applicableOHFList.indexOf(pli));
                        else {
                            ohfAdjustmentAmount += appliedOHFMap.get(pli)*-1;
                            Apttus__AgreementLineItem__c ali = new Apttus__AgreementLineItem__c(Id=ohfIdMap.get(pli));
                            ali.APTPS_Is_Included_In_OHF__c = false;
                            ali.APTPS_Is_OHF_Warranty_Date_Considered__c = true;
                            ohfIdSet.add(ohfIdMap.get(pli));
                            aliToUpdates.add(ali);
                        }
                        i+=1;
                    }
                }
                
                //Check if there is need to consider new OHF
                for(String pli : applicableOHFList) {
                    Decimal ohfAmoutToAdd = !allOHFLinesMap.isEmpty() && allOHFLinesMap.containsKey(pli) ? allOHFLinesMap.get(pli) : 0;
                    ohfAdjustmentAmount += ohfAmoutToAdd;
                    Apttus__AgreementLineItem__c ali = new Apttus__AgreementLineItem__c(Id=ohfIdMap.get(pli));
                    ali.APTPS_Is_Included_In_OHF__c = true;
                    ali.APTPS_Is_OHF_Warranty_Date_Considered__c = true;
                    ohfIdSet.add(ohfIdMap.get(pli));
                    aliToUpdates.add(ali);
                }
                
                system.debug('ohfAdjustmentAmount --> '+ohfAdjustmentAmount);
                if(ohfAdjustmentAmount != 0) {
                    Apttus__AgreementLineItem__c ali = new Apttus__AgreementLineItem__c(Id=ohfIdMap.get(APTS_ConstantUtil.OHF_CHARGE_TYPE));
                    ali.Apttus__NetPrice__c = allOHFLinesMap.get(APTS_ConstantUtil.OHF_CHARGE_TYPE) + ohfAdjustmentAmount;
                    ali.Apttus_CMConfig__BasePriceOverride__c = allOHFLinesMap.get(APTS_ConstantUtil.OHF_CHARGE_TYPE) + ohfAdjustmentAmount;
                    ali.APTPS_Is_OHF_Warranty_Date_Considered__c = true;
                    ohfIdSet.add(ohfIdMap.get(APTS_ConstantUtil.OHF_CHARGE_TYPE));
                    aliToUpdates.add(ali);
                }

                
                //Iterate ALIs and stamp Is Warranty Date available?
                for(Id recId : ohfIdMap.values()){
                    if(ohfIdSet != null && !ohfIdSet.contains(recId)) {
                        Apttus__AgreementLineItem__c ali = new Apttus__AgreementLineItem__c(Id=recId);
                        ali.APTPS_Is_OHF_Warranty_Date_Considered__c = true;
                        aliToUpdates.add(ali);
                    }
                }
            } else
                system.debug('Either OHF is not applied or Warranty Date is NULL');
            //End
        }
        system.debug('aliToUpdates --> '+aliToUpdates);
        if(!aliToUpdates.isEmpty())
            update aliToUpdates;
        
        //Redirect
        String url = '';
        url = getRedirectURL(userAction);
        pg = new PageReference(url);
        pg.setRedirect(true);
        return pg;
    }
    
    private String getRedirectURL(String action) {
        String url = '';
        String userTheme = UserInfo.getUiThemeDisplayed();
        if(userTheme.equalsIgnoreCase('Theme4d') || userTheme.equalsIgnoreCase('Theme4t') || userTheme.equalsIgnoreCase('Theme4u'))
            url = '/apex/Apttus__SelectTemplate?id='+agId+'&action='+action+'&templateType=Agreement';
        else 
            url = '/apex/Apttus__LightningSelectTemplate?id='+agId+'&action='+action+'&templateType=Agreement';
        return url;
    }
}