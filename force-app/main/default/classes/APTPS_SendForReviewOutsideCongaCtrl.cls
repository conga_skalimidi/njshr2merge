/**********************************************************************************************************************
@Name: APTPS_SendForReviewOutsideCongaCtrl
@Author: Avinash Bamane
@CreateDate: 09 August 2021
@Description : This  class is called to update 'Sent For Review Outside Conga' checkbox
***********************************************************************************************************************/
public class APTPS_SendForReviewOutsideCongaCtrl {
    @AuraEnabled
    public static string updateReviewOutsideCongaRequest(String recordId)
    {
        String msg = '';
        try {
            Apttus__APTS_Agreement__c ag = new Apttus__APTS_Agreement__c(Id=recordId, APTPS_Sent_For_Review_Outside_Conga__c=true);
            system.debug('Agreement to update --> '+ag);
            update ag;
            msg = 'Success';
        } catch(Exception e) {
            system.debug('Error while updating Sent for review outside conga status. Error details --> '+e);
            msg = 'Fail';
        }
        return msg;
    }
}