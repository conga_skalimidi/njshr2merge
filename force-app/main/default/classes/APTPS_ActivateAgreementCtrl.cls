/*************************************************************
@Name: APTPS_ActivateAgreementCtrl
@Author: Siva Kumar
@CreateDate: 02 July 2021
@Description : This  class is called to Activate SNL Agreement
******************************************************************/
public class APTPS_ActivateAgreementCtrl {
@AuraEnabled
    public static string activateAgreement(string recordId)
    {
        String msg = '';
        List<Attachment> files = new List<Attachment>();
        List<String> activateDocIds = new List<String>();
        List<Apttus__APTS_Agreement__c> agrList = [SELECT id,APTS_Activate_Click__c FROM Apttus__APTS_Agreement__c WHERE id=:recordId AND APTS_Activate_Click__c = false LIMIT 1];
         try {
                if(!agrList.isEmpty()){
                    files = [SELECT Id,Name, Body, ContentType FROM Attachment WHERE parentId = :recordId LIMIT 1];
                    if(!files.isEmpty()) {                          
                       activateDocIds.add(files[0].Id);                
                    }
                    else {              
                        Attachment dummyAttachment = new Attachment();
                        dummyAttachment.Name = 'Test';
                        dummyAttachment.Body = Blob.valueOf('Test');
                        dummyAttachment.ContentType = 'text/plain';
                        dummyAttachment.ParentId = recordId;
                        insert dummyAttachment;
                        activateDocIds.add(dummyAttachment.Id);             
                    }

                         
                    String[] remDocIds = new String[]{};
                    Boolean response = Apttus.AgreementWebService.activateAgreement(recordId, activateDocIds, remDocIds);
                    if(response) {
                        msg = 'Success';
                        Apttus__APTS_Agreement__c updateAgreement = new Apttus__APTS_Agreement__c(id=recordId,APTS_Activate_Click__c=true);
                        update updateAgreement;
                        
                    } else {
                        msg = 'Fail';
                    }
                }
                    
                    
            } catch(Exception e) {
                msg = 'Fail';
            }
        return msg;
    }
}