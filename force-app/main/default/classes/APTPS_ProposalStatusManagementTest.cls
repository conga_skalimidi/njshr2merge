/************************************************************************************************************************
@Name: APTPS_ProposalStatusManagementTest
@Author: Conga PS Dev Team
@CreateDate: 04 June 2021
@Description: 
************************************************************************************************************************
@ModifiedBy:
@ModifiedDate:
@ChangeDescription:
************************************************************************************************************************/

@isTest
public class APTPS_ProposalStatusManagementTest {
    @testSetup
    public static void makeData(){        
        Test.startTest();
        //Create SNL Custom settings
        APTPS_SNL_Products__c snlProducts = new APTPS_SNL_Products__c();
        snlProducts.Products__c = 'Demo,Corporate Trial';
        insert snlProducts;
        
        Account accToInsert = new Account();
        accToInsert.Name = 'APTS Test Account';
        insert accToInsert;
        
        Opportunity testOpportunity = APTS_CPQTestUtility.createOpportunity('test Apttus Opportunity', 'New Customer', accToInsert.Id, 'Propose');
        insert testOpportunity;
        
        Apttus_Config2__PriceList__c testPriceList = APTS_CPQTestUtility.createPriceList('test Apttus Price List', true);
        insert testPriceList;
        
        //Create Quote/Proposals
        List<Apttus_Proposal__Proposal__c> proposalList = new List<Apttus_Proposal__Proposal__c>();
        Apttus_Proposal__Proposal__c demoNJUSQuote = APTS_CPQTestUtility.createProposal('Quote/Proposal status management', accToInsert.Id, testOpportunity.Id, 'Proposal', testPriceList.Id);
        demoNJUSQuote.APTS_New_Sale_or_Modification__c = APTS_Constants.NEW_SALE;
        demoNJUSQuote.APTPS_Program_Type__c = APTS_ConstantUtil.DEMO;
        proposalList.add(demoNJUSQuote);
        
        insert proposalList;
        Test.stopTest();
    }
    
    @isTest
    public static void testProposalStatusManagement() {
        Test.startTest();
        Apttus_Proposal__Proposal__c qObj = [SELECT Id, Apttus_Proposal__Approval_Stage__c, Apttus_QPApprov__Approval_Status__c FROM Apttus_Proposal__Proposal__c 
                                             WHERE APTPS_Program_Type__c=:APTS_ConstantUtil.DEMO LIMIT 1];
        qObj.Apttus_Proposal__Approval_Stage__c = APTS_ConstantUtil.GENERATED;
        qObj.Apttus_QPApprov__Approval_Status__c = APTS_ConstantUtil.APPROVAL_REQUIRED;
        update qObj;
        
        qObj.Apttus_Proposal__Approval_Stage__c = APTS_ConstantUtil.IN_REVIEW;
        qObj.Apttus_QPApprov__Approval_Status__c = APTS_ConstantUtil.PENDING_APPROVAL;
        update qObj;
        
        qObj.Apttus_Proposal__Approval_Stage__c = APTS_ConstantUtil.APPROVED;
        qObj.Apttus_QPApprov__Approval_Status__c = APTS_ConstantUtil.APPROVED;
        update qObj;
        
        //Rejection scenario
        qObj.Apttus_Proposal__Approval_Stage__c = APTS_ConstantUtil.IN_REVIEW;
        qObj.Apttus_QPApprov__Approval_Status__c = APTS_ConstantUtil.PENDING_APPROVAL;
        update qObj;
        
        qObj.Apttus_Proposal__Approval_Stage__c = APTS_ConstantUtil.QUOTE_DRAFT;
        qObj.Apttus_QPApprov__Approval_Status__c = APTS_ConstantUtil.REJECTED;
        update qObj;
        
        Apttus_Proposal__Proposal__c resultObj = [SELECT Id, Proposal_Chevron_Status__c, Apttus_Proposal__Approval_Stage__c, Apttus_QPApprov__Approval_Status__c FROM Apttus_Proposal__Proposal__c 
                                                  WHERE ID=:qObj.Id LIMIT 1];
        system.assertEquals(APTS_ConstantUtil.PROPOSAL_SOLUTION, resultObj.Proposal_Chevron_Status__c);
        Test.stopTest();
    }
}