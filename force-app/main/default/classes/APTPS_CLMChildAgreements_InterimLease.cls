/************************************************************************************************************************
@Name: APTPS_CLMChildAgreements_InterimLease
@Author: Conga PS Dev Team
@CreateDate: 16 September 2021
@Description: 
************************************************************************************************************************
@ModifiedBy:
@ModifiedDate:
@ChangeDescription:
************************************************************************************************************************/

public class APTPS_CLMChildAgreements_InterimLease {
    /** 
    @description: ppoulate Agreeement Fields
    @param: AgreementList and Currency Code
    @return: 
    */
    private static void createChildAgreement( Set<String> agIds, String currencyCode) {
        List<Apttus__APTS_Agreement__c> interimLeaseAgreements = new List<Apttus__APTS_Agreement__c>();
        List<Apttus__APTS_Agreement__c> agList = new List<Apttus__APTS_Agreement__c>();
        List<String> dsFields = new List<String>();
        if(!agIds.isEmpty()) {
            system.debug('Total number of eligible records for Interim Lease --> '+agIds.size());
            
            String fieldSetName = APTS_ConstantUtil.INTERIM_LEASE_FIELD_SET;
            for(Schema.FieldSetMember fld : Schema.SObjectType.Apttus__APTS_Agreement__c.fieldSets.getMap().get(fieldSetName).getFields()) {
                String fldName = fld.getFieldPath();
                if(!dsFields.contains(fldName))
                    dsFields.add(fldName);
            }
            String query = 'SELECT Id,'+String.join(dsFields,',')+' FROM Apttus__APTS_Agreement__c WHERE ID IN :agIds';
            system.debug('SNL Interim Lease Agreement query --> '+query);
            agList = Database.query(query);
            for(Apttus__APTS_Agreement__c ag : agList) {
                Apttus__APTS_Agreement__c newAg = new Apttus__APTS_Agreement__c();
                for(String fieldName : dsFields) {
                    newAg.put(fieldName,ag.get(fieldName));
                }
                Id onHoldRecType = Schema.SObjectType.Apttus__APTS_Agreement__c.getRecordTypeInfosByDeveloperName().get(APTS_ConstantUtil.INTERIM_AGREEMENT_HOLD).getRecordTypeId();
                 system.debug('onHoldRecType --> '+onHoldRecType );
                newAg.Apttus__Parent_Agreement__c = ag.Id;
                newAg.RecordTypeId = onHoldRecType;
                newAg.Agreement_Status__c = APTS_ConstantUtil.AGREEMENT_ON_HOLD;
                newAg.Chevron_Status__c = APTS_ConstantUtil.AGREEMENT_ON_HOLD;
                newAg.Apttus__Status__c = APTS_ConstantUtil.REQUEST;
                newAg.Apttus__Status_Category__c = APTS_ConstantUtil.REQUEST;
                system.debug('newAg--> '+newAg);
                interimLeaseAgreements.add(newAg);
            }
            
            if(!interimLeaseAgreements.isEmpty()) {
                system.debug('Total number of interim lease agreements to insert --> '+interimLeaseAgreements.size());
                system.debug('interimLeaseAgreements --> '+interimLeaseAgreements);
                APTPS_Agreement_Helper.interimLeaseAgreementProcessed = true;
                insert interimLeaseAgreements;
            }
        }
    }
    
    /** 
    @description: NJE Demo implementation
    @param:
    @return: 
    */
    public class NJE_Agreement implements APTPS_CLMChildAgreementsInterface {
        public void updateChildAgreement(Set<String> args) {
            system.debug('updateChildAgreement');
            
              Set<String> agIds = args;
             APTPS_CLMChildAgreements_InterimLease.createChildAgreement(agIds,APTS_ConstantUtil.CUR_EUR);
            
                
            
        }
    }
    
    /** 
    @description: NJA Demo implementation
    @param:
    @return: 
    */
    public class NJA_Agreement implements APTPS_CLMChildAgreementsInterface {
        public void updateChildAgreement(Set<String> args) {
            system.debug('updateChildAgreement');
            
            Set<String> agIds = args;
             APTPS_CLMChildAgreements_InterimLease.createChildAgreement(agIds,APTS_ConstantUtil.CUR_USD);
            
                
            
        }
    }
}